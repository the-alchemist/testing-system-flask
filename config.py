import os

class Config:
    APP_SECRET_KEY = "MVwZ5n1zAEQHBYWpB5ed" # TODO: change this on yours
    APP_DIR = os.path.abspath(os.path.dirname(__file__))
    PROJECT_ROOT = os.path.abspath(os.path.join(APP_DIR, os.pardir))
    APP_CACHE_TYPE = "simple" # Can be "memcached", "redis", etc.
    STATIC_FOLDER = "app/static"

class DevConfig(Config):
    ENVIRONMENT = 'dev'
    DEBUG = True
    DB_NAME = 'rts.db'
    DB_PATH = os.path.join(Config.PROJECT_ROOT, DB_NAME)
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:graid363521!!!@localhost:5432/rts?client_encoding=utf8'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class ProdConfig(Config):
    ENVIRONMENT = 'prod'
    DEBUG = False
    DB_NAME = "rts.db"
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:graid363521!!!@localhost:5432/rts?client_encoding=utf8'