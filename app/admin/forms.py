from flask_wtf import FlaskForm as Form
from flask import flash
from wtforms import SubmitField
from ..auth.models import db, User

class UserControlForm(Form):
    remove = SubmitField("Remove")
    make_admin = SubmitField("Upgrade")

class AdminControlForm(Form):
    admin_remove = SubmitField("Remove")
    admin_make_user = SubmitField("Downgrade")

class QuizListForm(Form):
    SECRET_KEY = "quiz_list_form_secret_key"

    admin_quiz_edit = SubmitField("")
    admin_quiz_remove = SubmitField("")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)