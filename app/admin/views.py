# coding: utf-8
from flask import render_template, request, session, redirect, url_for, flash
from __init__ import admin_route as admin
from ..main.decorators import login_required, admin_required
from ..auth.models import db, User
from ..quiz.models import Quiz
from forms import UserControlForm, AdminControlForm, QuizListForm

@admin.route('/')
@login_required
@admin_required
def home():
    users = User.query.order_by(User.id.asc()).all()
    all_quizes = Quiz.query.order_by(Quiz.id.asc()).all()

    user = User.query.filter_by(email=session['email']).first()

    ucf = UserControlForm()
    acf = AdminControlForm()
    qlf = QuizListForm()

    return render_template('admin/admin.html', user=user, users=users, all_quizes=all_quizes, ucf=ucf, acf=acf, qlf=qlf)

@admin.route('/<user_id>/remove/', methods=['GET', 'POST'])
@login_required
@admin_required
def remove_user(user_id):
    ucf = UserControlForm()

    user = User.query.filter_by(id=user_id).first()
    if request.method == 'POST':
        db.session.delete(user)
        db.session.commit()
        flash("Success: Deleted user {0}".format(user.name))
        # session.pop('email', None)
        return redirect(url_for('admin.home'))

@admin.route('/<user_id>/downgrade/', methods=['GET', 'POST'])
@login_required
@admin_required
def downgrade_to_user(user_id):
    ucf = UserControlForm()

    user = User.query.filter_by(id=user_id).first()
    if request.method == 'POST':    
        user.make_user()
        db.session.commit()
        flash("Success: Changed rights of admin {0} to 'user'".format(user.name))
        return redirect(url_for('admin.home'))

@admin.route('/<user_id>/upgrade/', methods=['GET', 'POST'])
@login_required
@admin_required
def upgrade_to_admin(user_id):
    ucf = UserControlForm()

    user = User.query.filter_by(id=user_id).first()
    if request.method == 'POST':    
        user.make_admin()
        db.session.commit()
        flash("Success: Changed rights of user {0} to 'admin'".format(user.name))
        return redirect(url_for('admin.home'))