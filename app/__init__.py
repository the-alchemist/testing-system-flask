from flask import Flask
from extensions import db
from config import DevConfig

from main.__init__ import main_route
from auth.__init__ import auth_route
from quiz.__init__ import quiz_route
from admin.__init__ import admin_route

def create_app():
    app = Flask(__name__)

    ''' :param config_object: The configuration object to use. '''
    app.config.from_object(DevConfig)
    app.secret_key = DevConfig.APP_SECRET_KEY
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    return app

def register_extensions(app):
    db.init_app(app)
    return None

def register_blueprints(app):
    app.register_blueprint(main_route)
    app.register_blueprint(auth_route, url_prefix='/auth')
    app.register_blueprint(admin_route, url_prefix='/admin')
    app.register_blueprint(quiz_route, url_prefix='/quiz')
    return None

def register_errorhandlers(app):
    return None