from flask_wtf import FlaskForm as Form
from flask import flash
from wtforms import StringField, PasswordField, SubmitField, BooleanField, RadioField
from wtforms.validators import Required, EqualTo
from models import db, Quiz, Question, Answer

class QuizForm(Form):
    SECRET_KEY="quiz_form_secret_key"

    answers = RadioField('Label', choices=[('value', 'description'),('value_two', 'whatever')])
    submit = SubmitField("Answer")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)
