from ..extensions import db
import datetime
from sqlalchemy.orm import object_session

from ..auth.models import User, UserAssignedQuiz, QuizRelatedQuestion

class Quiz(db.Model):
    __tablename__ = "quizes"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    code = db.Column(db.String(16), nullable=False)
    
    # questions = db.relationship('Question', order_by='Question.id', backref = 'quiz', lazy = 'dynamic')

    user_assigned_quizes = db.relationship('UserAssignedQuiz', order_by='UserAssignedQuiz.id', backref = 'quiz', lazy = 'dynamic')
    quiz_related_questions = db.relationship('QuizRelatedQuestion', order_by='QuizRelatedQuestion.id', backref = 'quiz', lazy = 'dynamic')

    def flush(self):
        for item in self.quiz_related_questions:
            item.status = "hold"

    def __repr__(self):
        return "<Quiz {0}>".format(self.title)

    def __init__(self, title, hint):
        self.title = title
        self.code = title.split(' ')[0][0] + title.split(' ')[1][0] + "-" + self.uid

class Question(db.Model):
    __tablename__ = "questions"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    title = db.Column(db.String(128), nullable=False)
    hint = db.Column(db.String(128), nullable=False)

    quiz_related_questions = db.relationship('QuizRelatedQuestion', order_by='QuizRelatedQuestion.id', backref = 'question', lazy = 'dynamic')

    answers = db.relationship('Answer', order_by='Answer.id', backref = 'question', lazy = 'dynamic')
    attaches = db.relationship('Attach', order_by='Attach.id', backref = 'question', lazy = 'dynamic')

    def get_answers(self):
        return self.answers

    def get_attaches(self):
        return self.attaches

    def __repr__(self):
        return "<Question {0}, related to quiz {1}>".format(self.id, self.quiz_id)

    def __init__(self, title, hint, type, correct):
        self.title = title
        self.hint = hint
        self.type = type
        self.selected = ""
        self.correct = correct
        self.status = "hold"

class Answer(db.Model):
    __tablename__ = "answers"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    title = db.Column(db.String(64), nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('rts-schema.questions.id'))
    correct = db.Column(db.Boolean, nullable=False, default=False)
    selected = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return "<Answer {0}, related to question {1}>".format(self.id, self.question_id)

    def __init__(self, title):
        self.title = title

class Attach(db.Model):
    __tablename__ = "attaches"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    src = db.Column(db.String(255), nullable=False)
    question_id = db.Column(db.Integer, db.ForeignKey('rts-schema.questions.id'))

    def __repr__(self):
        return "<Attach {0}, related to question {1}>".format(self.id, self.question_id)

    def __init__(self, src):
        self.src = src

class Result(db.Model):
    __tablename__ = "results"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    result_string = db.Column(db.String(255), nullable=True)
    user_assigned_quiz_id = db.Column(db.Integer, db.ForeignKey('rts-schema.user_assigned_quizes.id'))

    def set_result(self, id, result):
        self.user_assigned_quiz_id = id
        self.result_string = result
    
    def __init__(self, id, result):
        self.user_assigned_quiz_id = id
        self.result_string = result

    def __repr__(self):
        return "<Result {0}, related to user {1}>".format(self.id, self.user_assigned_quiz_id)