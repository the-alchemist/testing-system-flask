from __future__ import print_function
import datetime
import time

from flask import render_template, flash, request, session, redirect, url_for
from __init__ import quiz_route
from ..quiz.models import Quiz, Question, Result, db
from ..auth.models import User, UserAssignedQuiz, QuizRelatedQuestion
from forms import QuizForm
from ..main.decorators import admin_required, login_required


@login_required
@quiz_route.route('/<quiz_id>/')
def quiz(quiz_id):
    quiz_form = QuizForm()

    user = User.query.filter_by(email=session['email']).first()
    assigned_quiz = user.get_assigned_quiz(quiz_id)
    
    if request.method == 'GET':
        if assigned_quiz.status == "completed":
                return redirect(url_for('quiz.completed', quiz_id=quiz_id))
        else:
            # Quiz starts
            assigned_quiz.start()
            related_questions = assigned_quiz.quiz.quiz_related_questions.paginate(page=assigned_quiz.quiz.quiz_related_questions.count(), per_page=1)
            current_question = related_questions.query.first()
            current_question.set_active()
            return render_template('quiz/quiz.html', form=quiz_form, assigned_quiz=assigned_quiz, user=user, current_question=current_question)
        #         return redirect(url_for('main.home'))
        

@login_required
@admin_required
@quiz_route.route('/<quiz_id>/edit/assign/', methods=['GET','POST'])
def assign_quiz(quiz_id):

    current_quiz = Quiz.query.filter_by(id=quiz_id).first()
    userid = request.form['quiz_edit_adduser_field']
    assigned_quiz = UserAssignedQuiz.query.filter_by(user_id=userid).first()

    user = User.query.filter_by(id=userid).first()

    if request.method == 'POST':
        if 'all' in request.form['quiz_edit_assign_all']:
            all_users = User.query.all()
            for user in all_users:
                new_assignation = UserAssignedQuiz(user.id, current_quiz.id, "hold", None, None, 0, 0)
                db.session.add(new_assignation)
                db.session.commit()
            flash(u'Successfully assigned quiz "{0}" to all users!'.format(current_quiz.title))
            return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))
        else:
            if not assigned_quiz:
                new_assignation = UserAssignedQuiz(userid, current_quiz.id, "hold", None, None, 0, 0)
                db.session.add(new_assignation)
                db.session.commit()
                flash(u'Successfully assigned quiz "{0}" to user "{1}"!'.format(user.id, current_quiz.title))
                return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))
            else:
                flash('Error: Already assigned!')
                return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))
    else:
        flash('Error: Only POST method is allowed!')
        return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))

@login_required
@admin_required
@quiz_route.route('/<quiz_id>/edit/deassign/<user_id>/', methods=['GET', 'POST'])
def deassign_quiz(quiz_id, user_id):

    current_quiz = Quiz.query.filter_by(id=quiz_id).first()
    assigned_quiz = UserAssignedQuiz.query.filter_by(user_id=user_id).first()

    user = User.query.filter_by(id=user_id).first()

    if request.method == 'POST':
        if assigned_quiz:
            db.session.delete(assigned_quiz)
            db.session.commit()
            flash(u'Successfully deassigned quiz "{0}" off user "{1}"'.format(current_quiz.title, user.name))
            return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))
        else:
            flash('Error: No quiz dessigned!')
            return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))
    else:
        flash('Error: Only POST method is allowed!')
        return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))

@login_required
@quiz_route.route('/<quizid>/question/<question_id>/answer/', methods=['GET','POST'])
def answer_quiz(quizid, question_id):
    quiz_form = QuizForm()

    user = User.query.filter_by(email=session['email']).first()
    assigned_quiz = user.get_assigned_quiz(quizid)

    questions = assigned_quiz.quiz.quiz_related_questions.paginate(page=assigned_quiz.quiz.quiz_related_questions.count(), per_page=1)
    question = questions.query.first()

    result = Result.query.filter_by(user_assigned_quiz_id=assigned_quiz.user_id).first()

    if request.method == 'POST':
        if not question is None:
            if question.type == "single":
                user_answer = request.form['radio_answer']
            elif question.type == "multi":
                user_answer = request.form['checkbox_answer']
            
            question.answer(user_answer)

            # if result is None:
            #     new_result = Result(assigned_quiz.quiz_id, user_answer)
            #     db.session.add(new_result)
            #     db.session.commit()
            # else:
            #     result.set_result(assigned_quiz.quiz_id, user_answer)
            #     db.session.commit()

            if question:
                next_question = questions.next()
                assigned_quiz.quiz.flush()
                next_question.items.first().set_active()
            else:
                assigned_quiz.finish()
                return redirect(url_for('quiz.completed', quiz_id=assigned_quiz.quiz_id))
            db.session.commit()
            return render_template('quiz/quiz.html', form=quiz_form, assigned_quiz=assigned_quiz, current_question=next_question, user=user)
    elif request.method == 'GET':
        return redirect(url_for('quiz.quiz', quiz_id=assigned_quiz.quiz_id))

@login_required
@quiz_route.route('/<quiz_id>/completed/')
def completed(quiz_id):
    user = User.query.filter_by(email=session['email']).first()
    assigned_quiz = UserAssignedQuiz.query.filter_by(quiz_id=quiz_id).first()

    if assigned_quiz.status == "completed":
        return render_template('quiz/completed.html', user=user, assigned_quiz=assigned_quiz)
    else:
        return redirect(url_for('quiz.quiz', quiz_id=assigned_quiz.id))

@login_required
@quiz_route.route('/<quiz_id>/create-report-<report_type>')
def create_report(quiz_id, report_type):
    if(report_type in ["xml"]):
        report = make_xml_report()
    elif(report_type in ["excel", "xls"]):
        report = make_xls_report()
    elif(report_type in ["doc", "word"]):
        report = make_doc_report()
    else:
        flash('Error: This filetype of report is not supported!')
        return redirect(url_for('quiz.completed', quiz_id=quiz_id))
    


@login_required
@admin_required
@quiz_route.route('/<quiz_id>/edit/', methods=['GET', 'POST'])
def edit_quiz(quiz_id):

    current_quiz = Quiz.query.filter_by(id=quiz_id).first()
    users = User.query.order_by(User.id.asc()).all()

    assigned_users = UserAssignedQuiz.query.filter_by(quiz_id=quiz_id).all()

    if request.method == 'POST':
            # your action here
            db.session.commit()
            return redirect(url_for('main.home'))
    elif request.method == 'GET':
            # if quiz:
            #         return redirect(url_for('quiz.quiz'))
            # else:
            #         return redirect(url_for('main.home'))
            return render_template('quiz/edit.html', current_quiz=current_quiz, assigned_users=assigned_users, users=users)

@login_required
@admin_required
@quiz_route.route('/<quiz_id>/remove-quiz/')
def remove_quiz(quiz_id):
    
    quiz = Quiz.query.filter_by(id=quiz_id).first()

    if request.method == 'POST':
        db.session.delete(quiz)
        db.session.commit()
        flash(u'Successfully deassigned quiz "{0}" off user "{1}"'.format(current_quiz.title, user.name))
        return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))

@login_required
@admin_required
@quiz_route.route('/<quizid>/remove-question/<questionid>/', methods=['GET', 'POST'])
def remove_question(quizid, questionid):
    
    related_question = QuizRelatedQuestion.query.filter_by(quiz_id=quizid).filter_by(question_id=questionid).first()

    if request.method == 'POST':
        if related_question:
            # Achtung! Removing by CASCADE!
            db.session.delete(related_question.question)
            db.session.delete(related_question)
            db.session.commit()
            flash(u'Successfully removed question "{0}" from quiz "{1}"'.format(related_question.question_id, current_quiz.title))
            return redirect(url_for('quiz.edit_quiz', quiz_id=current_quiz.id))
    elif request.method == 'GET':
        flash('Only POST method is allowed!')
        return redirect(url_for('admin.home'))


@login_required
@admin_required
@quiz_route.route('/<quiz_id>/completed/details/')
def quiz_detail(quiz_id):
    quiz = Quiz.query.order_by(Quiz.id.asc()).filter_by(id=quiz_id).first()
    users = User.query.order_by(User.id.asc()).all()

    if request.method == 'POST':
            # your action here
            db.session.commit()
            return redirect(url_for('main.home'))
    elif request.method == 'GET':
            # if quiz:
            #         return redirect(url_for('quiz.quiz'))
            # else:
            #         return redirect(url_for('main.home'))
            return render_template('quiz/detail.html', quiz=quiz, users=users)