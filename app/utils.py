from __future__ import print_function
from auth.models import db
import getpass
import subprocess

def start_database():
    try:
        subprocess.Popen(["service", "postgresql", "start"])
        # print('Starting postgresql db from server...', end="")
    finally:
        print('')

def start_node():
    name = subprocess.check_output(["node", "app/static/js/app.js"])
    print(name)

def test_database():
    if db.session.query("1").from_statement("SELECT 1").all():
        return 'works'
    else:
        return 'broken'

def getcurrentuser():
    return getpass.getuser()

def getcurrentuseranddomain():
    return subprocess.check_output("whoami").replace("\r\n", "")

def make_xml_report(data):
    import xml.etree.ElementTree as ET
    # data = {'quiz_title':'Computer management', 'user':'lorem', 'score': '2', 'percentage': '50', 'questions': []}

    # questions = []

    # question_1 = {'question_title': 'Sample question 1', 'user_answer': 'User answer', 'answer_correct': 'no'}
    # question_2 = {'question_title': 'Sample question 2', 'user_answer': 'User correct answer', 'answer_correct': 'yes'}

    # questions.append(question_1)
    # questions.append(question_2)

    # data['questions'] = questions

    root = ET.Element('Report', {'user': data['user'], 'quiz': data['quiz_title']})
    quiz = ET.SubElement(root, 'Quiz', {'id': '1', 'title': data['quiz_title'], 'status': 'completed', 'assignedAt': '2018-01-01 12:55:33', 'completedAt': '2018-01-01 16:04:21', 'score': data['score'], 'percentage': data['percentage']})
    questions = ET.SubElement(quiz, 'Questions')
    for i in range(len(data['questions'])):
        question = ET.SubElement(questions, 'Question', {'type': 'single', 'id': '1', 'title': data['questions'][i]['question_title']})
        useranswer = ET.SubElement(question, 'UserAnswer', { 'id': '16', 'correct': data['questions'][i]['answer_correct']})
        useranswer.text = data['questions'][i]['user_answer']

    tree = ET.ElementTree(root)
    filename = 'result_{0}_{1}.xml'.format(data['quiz_title'], data['user'])
    with open(filename, "w") as report_xml:
        tree.write(report_xml)


def make_xls_report(data):
    from openpyxl import Workbook

    # data = {'quiz_title':'Computer management', 'user':'lorem', 'score': '2', 'percentage': '50', 'questions': []}

    # questions = []

    # question_1 = {'question_title': 'Sample question 1', 'user_answer': 'User answer', 'answer_correct': 'no'}
    # question_2 = {'question_title': 'Sample question 2', 'user_answer': 'User correct answer', 'answer_correct': 'yes'}

    # questions.append(question_1)
    # questions.append(question_2)

    # data['questions'] = questions

    wb = Workbook()

    sheet = wb.active

    sheet.merge_cells('A1:C1')

    sheet['A1'] = 'Results of {0} test completion'.format(data['quiz_title'])

    sheet['A3'] = 'User'
    sheet['B3'] = data['user']

    sheet['A5'] = 'Score'
    sheet['B5'] = data['score']

    sheet['A6'] = 'Percentage'
    sheet['B6'] = "{0}%".format(data['percentage'])

    sheet['A10'] = 'Question title'
    sheet['B10'] = 'User answer'
    sheet['C10'] = 'Correct answer'

    current_col = 11

    for i in range(len(data['questions'])):
        sheet.cell(row=i+current_col, column=1).value = data['questions'][i]['question_title']
        sheet.cell(row=i+current_col, column=2).value = data['questions'][i]['user_answer']
        sheet.cell(row=i+current_col, column=3).value = data['questions'][i]['answer_correct']

    wb.save('result_{0}_{1}.xlsx'.format(data['quiz_title'], data['user']))



def make_doc_report(data):
    from docx import Document
    from docx.shared import Inches

    # data = {'quiz_title':'Computer management', 'user':'lorem', 'score': '2', 'percentage': '50', 'questions': []}

    # questions = []

    # question_1 = {'question_title': 'Sample question 1', 'user_answer': 'User answer', 'answer_correct': 'no'}
    # question_2 = {'question_title': 'Sample question 2', 'user_answer': 'User correct answer', 'answer_correct': 'yes'}

    # questions.append(question_1)
    # questions.append(question_2)

    # data['questions'] = questions

    document = Document()

    document.add_heading('Results of {0} test completion'.format(data['quiz_title']), 0)
    document.add_heading('User: {0}'.format(data['user']), level=1)
    document.add_heading('Score: {0}'.format(data['score']), level=1)
    document.add_heading('Percentage: {0}'.format(data['percentage']), level=1)

    document.add_heading('Statistics', level=1)

    table = document.add_table(rows=len(data['questions']), cols=3)
    for i in range(len(data['questions'])):
        rows = table.rows[i]
        rows.cells[0].text = data['questions'][i]['question_title']
        rows.cells[1].text = data['questions'][i]['user_answer']
        rows.cells[2].text = data['questions'][i]['answer_correct']

    document.add_page_break()
    document.save('result_{0}_{1}.docx'.format(data['quiz_title'], data['user']))
