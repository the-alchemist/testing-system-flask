from flask import render_template, request, session, redirect, url_for, flash
from __init__ import main_route as main
from ..auth.__init__ import auth_route as auth
from ..auth.models import db, User
from ..quiz.models import Quiz
from ..auth.forms import LoginForm, RegistrationForm
from decorators import login_required
from ..utils import getcurrentuser

@main.route('/')
def home():
    return render_template("index.html")

# Leaving for backward compatibility

@main.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    user = User.query.filter_by(email=form.email.data).first()
    if request.method == 'POST':
        if not form.validate():
            return render_template('auth/login.html', form=form)
        else:
            if user and user.check_password(form.password.data):
                session['email'] = form.email.data
                flash("Success: Logged in!")
                return redirect(url_for('auth.profile'))
            else:
                flash("Error: Incorrect password!")
                return render_template('auth/login.html', form=form)
    elif request.method == 'GET':
        return render_template('auth/login.html', form=form)

@main.route('/register/', methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if request.method == 'POST' and form.validate():
            new_user = User(form.name.data, form.email.data, form.password.data, form.departament.data)
            db.session.add(new_user)
            db.session.commit()
            session['email'] = new_user.email
            flash("Successfully registered!")
            return redirect(url_for('auth.profile'))
    else:
        return render_template('auth/register.html', form=form)

# Prime login route 
# @main.route('/login/', methods=['GET', 'POST'])
# def login():
#     if request.method == 'GET':
#         user_name = getcurrentuser()
#         user_email = user_name + "@nfmz.ru"
#         new_user = User.query.filter_by(email=user_email).first()
#         if not new_user:
#             new_user = User(user_name, user_email, user_email, "", False)
#             db.session.add(new_user)
#             db.session.commit()
#         session['email'] = new_user.email
#         flash("Welcome, " + user_name + "!")
#         return redirect(url_for('auth.profile'))

@main.route('/quizes/')
@login_required
def quizes():
    user = User.query.filter_by(email=session['email']).first()
    assigned_quizes = user.get_assigned_quizes()
    all_quizes = Quiz.query.all()

    return render_template('quiz/list.html', user=user, assigned_quizes=assigned_quizes, all_quizes=all_quizes)


@main.errorhandler(404)
def page_not_found():
    return render_template("errors/404.html"), 404