from functools import wraps
from ..auth.models import User
from flask import session, redirect, url_for, flash
from __init__ import main_route as main
from ..auth.__init__ import auth_route as auth

def login_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'email' in session:
            return f(*args, **kwargs)
        else:
            flash("Error: You need to login first!")
            return redirect(url_for('main.login'))
    return wrap

def admin_required(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        user = User.query.filter_by(email=session['email']).first()
        admin_role = user.admin()
        if 'email' in session:
            if admin_role == "True":
                return f(*args, **kwargs)
            else:
                flash('Error: You have no permission to do this!')
                return redirect(url_for('auth.profile'))
        else:
            flash('Error: You need to login first!')
            return redirect(url_for('main.login'))
    return wrap