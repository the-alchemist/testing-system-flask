# coding: utf-8
from flask import render_template, request, session, redirect, url_for, flash
from forms import LoginForm, RegistrationForm, ProfileChangeForm
from __init__ import auth_route as auth
from werkzeug import generate_password_hash, check_password_hash
from models import User, UserAssignedQuiz, db
from ..main.decorators import login_required, admin_required

@auth.route('/unregister/', methods=['GET','POST'])
@login_required
def unregister():
    form = ProfileChangeForm()
    user = User.query.filter_by(email=session['email']).first()
    if request.method == 'POST':	
        db.session.delete(user)
        db.session.commit()
        return redirect(url_for('auth.logout'))
    elif request.method == 'GET':
        if user:
            return redirect(url_for('auth.profile'))
        else:
            session.pop('email', None)	
            return redirect(url_for('main.home'))

@auth.route('/changepwd/', methods=['GET','POST'])
@login_required
def changepwd():
    form = ProfileChangeForm()
    user = User.query.filter_by(email=session['email']).first()
    if request.method == 'POST':
        if(check_password_hash(user.password_hash, form.old_password.data)):
            user.set_password(form.new_password.data)
            db.session.commit()
            flash("Success: Password changed!")
            return redirect(url_for('auth.profile'))
        else:
            flash("Error: Old password is incorrect!!!")
            return redirect(url_for('auth.profile'))
    elif request.method == 'GET':
        return redirect(url_for('auth.profile'))

# @auth.route('/generate', methods=['GET','POST'])
# def generate():
#     form = ProfileChangeForm()
#     user = User.query.filter_by(email=session['email']).first()
#     if request.method == 'POST':
#         remained = user.get_tokens_remained()
#         if int(remained) == 0 and user.get_role() != 0:
#             flash("<p class=\"error\">You have reached your limit of tokens.</p>")
#             return redirect(url_for('auth.profile'))
#         else:
#             user.generate_token()
#             if user.get_role() != 0:
#                 user.reduce_tokens_count()
#             db.session.commit()
#             flash("New token was generated!")
#             return redirect(url_for('auth.profile'))
#     elif request.method == 'GET':
#         return redirect(url_for('auth.profile'))


@auth.route('/profile/')
@login_required
def profile():
    form = ProfileChangeForm()
    if 'email' not in session:
        return redirect(url_for('main.login'))
    user = User.query.filter_by(email=session['email']).first()
    userquiz = UserAssignedQuiz.query.filter_by(user_id=user.id).all()
    if user is None:
        return redirect(url_for('main.login'))
    else:
        return render_template('auth/profile.html', form=form, user=user, userquiz=userquiz)

@auth.route('/logout/')
@login_required
def logout():
    if 'email' not in session:
        return redirect(url_for('main.login'))
    session.pop('email', None)
    return redirect(url_for('main.home'))
