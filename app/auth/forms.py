from flask_wtf import FlaskForm as Form
from flask import flash
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired
from models import db, User

class LoginForm(Form):
    SECRET_KEY="login_form_secret_key"
    email = StringField("Email", validators=[DataRequired("Enter email!")])
    password = PasswordField("Password", validators=[DataRequired("Enter password!")])
    submit = SubmitField("Login")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False

        user = User.query.filter_by(email=self.email.data.lower()).first()
        if user and user.check_password(self.password.data):
            return True
        else:
            flash("Error: User " + self.email.data.lower() + "is not exist!")
            return False

class RegistrationForm(Form):
    SECRET_KEY="reg_form_secret_key"
    email = StringField("Email", validators=[DataRequired("Enter email!")])
    name = StringField("Nickname", validators=[DataRequired("Enter nickname!")])
    departament = StringField("Departament", validators=[DataRequired("Enter departament!")])
    password = PasswordField("Password", validators=[DataRequired("Enter password!")])
    confirm = PasswordField("Confirm password", validators=[DataRequired("Confirm password")])
    submit = SubmitField("Register")

    def __init__(self, *args, **kwargs):
        Form.__init__(self, *args, **kwargs)

    def validate(self):
        if not Form.validate(self):
            return False
        user = User.query.filter_by(email=self.email.data.lower()).first()
        if user:
            flash("Error: This email already taken!")
            return False
        elif self.password.data.lower() != self.confirm.data.lower():
            flash("Error: Passwords not match!")
            return False
        else:
            return True

class ProfileChangeForm(Form):
    old_password = PasswordField("Old password", validators=[DataRequired("Enter old password!")])
    new_password = PasswordField("New password", validators=[DataRequired("Enter new password!")])
    confirm = PasswordField("Confirm password", validators=[DataRequired("Confirm new password!")])
    submit = SubmitField("Change password")
    unregister = SubmitField("Unregister me")
    generate_token = SubmitField("Generate token")
    