import uuid
from ..extensions import db
from werkzeug import generate_password_hash, check_password_hash
from sqlalchemy.orm import object_session
import datetime

class User(db.Model):
    __tablename__ = "users"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(16), nullable=False)
    email = db.Column(db.String(32), nullable=False, unique=True)
    departament = db.Column(db.String(64), nullable=False)
    role = db.Column(db.String, nullable=True, default="user")
    password_hash = db.Column(db.String(255), nullable=True)

    user_assigned_quizes = db.relationship('UserAssignedQuiz', order_by='UserAssignedQuiz.id', backref = 'user', lazy = 'dynamic')

    def make_admin(self):
        self.role = "admin"

    def make_user(self):
        self.role = "user"

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_id(self):
        return self.id
    
    def find_by_id(self, user_id):
        return object_session(self).query(User).filter_by(User.id == user_id).order_by(User.id).first()

    def admin(self):
        return str(self.role == "admin")

    def get_name(self):
        return self.name

    def get_email(self):
        return self.email

    def get_departament(self):
        return self.departament

    def get_assigned_quizes(self):
        return self.user_assigned_quizes

    def get_assigned_quiz(self, quiz_id):
        return object_session(self).query(UserAssignedQuiz).filter(UserAssignedQuiz.quiz_id == quiz_id, UserAssignedQuiz.user_id == User.id).order_by(UserAssignedQuiz.quiz_id).first()

    def __repr__(self):
        return '<User id:{0} name:{1}>'.format(self.id, self.name)

    def __init__(self, name, email, password, departament):
        self.name = name
        self.email = email
        self.set_password(password)
        self.departament = departament
        self.is_admin = False

class UserAssignedQuiz(db.Model):
    __tablename__ = "user_assigned_quizes"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('rts-schema.users.id'))
    quiz_id = db.Column(db.Integer, db.ForeignKey('rts-schema.quizes.id'))
    status = db.Column(db.String(64), nullable=False)
    assigned = db.Column(db.String(64), nullable=False)
    completed = db.Column(db.String(64), nullable=False)
    score = db.Column(db.SmallInteger, default=0)
    percentage = db.Column(db.SmallInteger, default=0)

    results = db.relationship('Result', order_by='Result.id', backref = 'user', lazy = 'dynamic')

    def start(self):
        self.status = "active"
        self.assigned = str(datetime.datetime.now())

    def finish(self):
        self.status = "completed"
        self.completed = str(datetime.datetime.now())

    def calc_percentage(self):
        return self.calc_score(100)

    def calc_fifth(self):
        return self.calc_score(5)
    
    def calc_score(self, floor):
        correct_questions_count = 0
        all_questions_count = len(self.get_questions_for_quiz())

        for question in self.get_questions_for_quiz():
            for answer in question.get_answers():
                if 'True' in str(answer.correct):
                    correct_questions_count += 1

        return correct_questions_count / all_questions_count * floor

    def __init__(self, user_id, quiz_id, status, assigned, completed, score, percentage):
        self.user_id = user_id
        self.quiz_id = quiz_id
        self.status = 'hold'
        self.assigned = None
        self.completed = None
        self.score = 0
        self.percentage = 0

    def __repr__(self):
        return "<User {0} related to quiz {1}>".format(self.user_id, self.quiz_id)

class QuizRelatedQuestion(db.Model):
    __tablename__ = "quiz_related_questions"
    __table_args__ = {"schema":"rts-schema"}

    id = db.Column(db.Integer, nullable=False, primary_key=True)
    question_id = db.Column(db.Integer, db.ForeignKey('rts-schema.questions.id'))
    quiz_id = db.Column(db.Integer, db.ForeignKey('rts-schema.quizes.id'))
    status = db.Column(db.String(64), nullable=False)
    type = db.Column(db.String(64), nullable=False)

    def next(self):
        return object_session(self).query(QuizRelatedQuestion).filter(QuizRelatedQuestion.id > self.id).order_by(QuizRelatedQuestion.id).first()

    def has_next(self):
        return str(not 'None' in self.next().__dict__)

    def prev(self):
        return object_session(self).query(QuizRelatedQuestion).filter(QuizRelatedQuestion.id < self.id).order_by(desc(QuizRelatedQuestion.id)).first()

    # FLASK JINJA USAGE ONLY!!!!
    def check_correct(self):
        for item in self.question.answers:
            if item.question_id == self.id:
                if item.correct:
                    return 'True'
                else:
                    return 'False'
            else:
                return 'False'
    
    def check_correct_in_db(self):
        for item in self.question.answers:
            if item.question_id == self.id:
                if item.correct:
                    return True
                else:
                    return False
            else:
                return False

    def answer(self, _answer):
        for item in self.question.answers:
            if item.question_id == self.id:
                if _answer in item.title:
                    item.selected == True
                    item.correct = self.check_correct_in_db()

    def get_status(self):
        return self.status

    def set_active(self):
        self.status = "active"

    def __init__(self, question_id, quiz_id, type):
        self.question_id = question_id
        self.quiz_id = quiz_id
        self.status = "hold"
        self.type = type
    
    def __repr__(self):
        return "<Question {0}, related to quiz {1}>".format(self.question_id, self.quiz_id)