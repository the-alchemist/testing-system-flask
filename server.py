# -*- coding: UTF-8 -*-

from app import create_app
from tornado.wsgi import WSGIContainer
from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from app.utils import start_database, start_node

import sys

server_port = sys.argv[1]

server = HTTPServer(WSGIContainer(create_app()))
start_database()
start_node() 
if len(sys.argv[1]):
    server.listen(server_port)
    print "\nTornado Server is running @ localhost:{}".format(server_port)
else:
    server.listen(5000)
    print "\nTornado Server is running @ localhost:5000 (by default)"

IOLoop.instance().start()


